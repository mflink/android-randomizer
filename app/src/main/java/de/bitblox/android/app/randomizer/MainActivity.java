package de.bitblox.android.app.randomizer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnCoin = null;
    private Button btnDice06 = null;
    private Button btnDice20 = null;
    private Button btnRndChar = null;
    private Button btnUserDef = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCoin = (Button) findViewById(R.id.btncoin);
        btnCoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                int value = (int) (Math.random() * 100) + 1;
                Toast.makeText(getApplicationContext(), (value % 2) == 0 ? "head" : "tail", Toast.LENGTH_SHORT).show();
            }
        });

        btnDice06 = (Button) findViewById(R.id.btndice06);
        btnDice06.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                int value = (int) (Math.random() * 6) + 1;
                Toast.makeText(getApplicationContext(), String.valueOf(value), Toast.LENGTH_SHORT).show();
            }
        });

        btnDice20 = (Button) findViewById(R.id.btndice20);
        btnDice20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                int value = (int) (Math.random() * 20) + 1;
                Toast.makeText(getApplicationContext(), String.valueOf(value), Toast.LENGTH_SHORT).show();
            }
        });

        btnRndChar = (Button) findViewById(R.id.btnrndchar);
        btnRndChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String chars = "QWERTZUIOPASDFGHJKLYXCVBNM";
                int index = (int) (Math.random() * chars.length()) + 1;
                String value = chars.substring(index - 1, index);
                Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT).show();
            }
        });

        btnUserDef = (Button) findViewById(R.id.btnuserdef);
        btnUserDef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

            }
        });
    }
}
